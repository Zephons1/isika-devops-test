package fr.isika.al4.testsUnitaires;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MathsTest {

	Maths maths = new Maths();

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		//test que 1 + 1 = 2
		Assert.assertEquals(2, maths.additionner("1", "1"));

		//test que 1 + "   " = 1
		Assert.assertEquals(1, maths.additionner("1", "   "));

		//test que () + () => erreur
		Assert.assertEquals(0, maths.additionner());

		//test que "a" + 1 => erreur
		Assert.assertEquals("Erreur", maths.additionner("a", "1"));

	}
}
