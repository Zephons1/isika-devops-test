package fr.isika.al4.testsUnitaires;

public class Maths {

	private String historique;

	public Object additionner(String s1, String s2) {
		Integer i1 = tryParseInt(s1.trim());
		Integer i2 = tryParseInt(s2.trim());
		if (i1 != null && i2 != null) 
		{
			historique = s1 + " + " + s2;
			return i1 + i2;
		}
		else if (s1.trim().equals("")) {
			historique = s1.trim() + " + " + s2;
			return i2;
		}
		else if (s2.trim().equals("")) {
			historique = s1 + " + " + s2.trim();
			return i1;
		}
		else {
			historique = "Erreur";
			return historique;
		}
	}

	public Object additionner(String s) {
		Integer i = tryParseInt(s.trim());
		if (i != null) return i;
		else if (s.trim().equals("")) return 0;
		else return "Erreur";
	}

	public int additionner() {
		return 0;
	}

	public static Integer tryParseInt(String s) {
		try {
			return Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	public String getHistorique() {
		return historique;
	}

	public void setHistorique(String historique) {
		this.historique = historique;
	}
}
