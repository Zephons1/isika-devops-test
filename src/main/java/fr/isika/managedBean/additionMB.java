package fr.isika.managedBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import fr.isika.al4.testsUnitaires.Maths;

@ManagedBean
@ViewScoped
public class additionMB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8014034730827731504L;
	
	private Maths maths = new Maths();
	private List<String> listeAdditions = new ArrayList<String>();
	private String nombre1;
	private String nombre2;
	
	@PostConstruct
	private void init() {
		
	}

	public Object add(String nombre1, String nombre2) {
		Object resultat = maths.additionner(nombre1, nombre2);
		listeAdditions.add(maths.getHistorique());
		return resultat;
	}
	
	public Object add(String nombre1) {
		return maths.additionner(nombre1);
	}
	
	public int add() {
		return maths.additionner();
	}

	public String getNombre1() {
		return nombre1;
	}

	public void setNombre1(String nombre1) {
		this.nombre1 = nombre1;
	}

	public String getNombre2() {
		return nombre2;
	}

	public void setNombre2(String nombre2) {
		this.nombre2 = nombre2;
	}

	public List<String> getListeAdditions() {
		return listeAdditions;
	}

	public void setListeAdditions(List<String> listeAdditions) {
		this.listeAdditions = listeAdditions;
	}
}
